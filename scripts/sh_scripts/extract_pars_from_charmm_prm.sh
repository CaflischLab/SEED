#!/bin/bash
# This script is part of SEED
# It extracts the atom type and LJ parameters from a Charmm .prm file (e.g. par_all36_na.prm)
#
# TODO: fix hard-coded variables

if [[ -z "${1}" ]]; 
then
  echo -e "Usage:\n\n/bin/bash extract_pars_from_charmm_prm.sh PRMFILE [startidx] > OUTPUTFILE\n"
  exit
fi

if [[ -z "${2}" ]];
then
  startidx=1
else
  startidx="${2}"
fi


prmfile="${1}" 

sed -n -E '/NONBONDED\s+nbxmod/I,/^NBFIX$|^end$|^END$/{/^$/d; /^NONBONDED\s+nbxmod/Id; /^\s*cutnb/Id; /^NBFIX$/Id; /^end$/Id; /^!/d; /^\s+!/d; p; }' $prmfile | sed -e 's/!/#/' | awk -v startidx=${startidx} '
  BEGIN{
    atommap["O"] = 8;
    atommap["H"] = 1;
    atommap["N"] = 7;
    atommap["C"] = 6;
    atommap["P"] = 15;
    #startidx = 1;
  }
  {
    foundcomment = 0;
    printf "%-6d %-6s %-4d %7f %8f ", startidx, $1, atommap[substr($1,1,1)], $4, $3 * -1 ;
    for (i=1;i<NF;i++){
      if (substr($i,1,1) == "#"){
	foundcomment = 1;
	break;
      }
    }
    if (foundcomment == 1){
      s = ""; for (j = i; j <= NF; j++) s = s $j " "; printf "%s", s ; 
    }
    printf "\n";
    startidx++;
  }
'

# for combining together after concatenation:
# cut blabla.par -d ' ' -f2- | sed -E 's/^\s+//' | awk '{printf "%-4d %s\n", NR, $0}'
