#!/usr/bin/python
"""Clangini 09/2023
This script aims at converting a mol2 file to a mol2 "seed" format,
copying the sybyl atom types into the alternative atom types section.
"""
import sys, os
if len(sys.argv)!=3:
    print("Usage = python script.py receptor_in.mol2 receptor_out.mol2")
    sys.exit()

# Open data
a=open(sys.argv[1], "r")
orimol2 = a.readlines()
a.close()

## Process the mol2 file
# The default atom charges need to be updated to CHARMM charges
# We also need to generate a dict of ALT_TYPE with atom indice
# as the key and the value is the CHARMM atom type
whereami_mol2=False
start=False
james_bond=False
list_atom_types = []
res=False
out=""

for line in orimol2:
    if len(line.strip().split())==0: # empty line
        continue
    elif line.strip().startswith("#"): # comment line
        continue
    else:
        if "@<TRIPOS>ATOM" in line:
            start=True
            whereami_mol2=True
            out+=line
            continue
        
        if start==True:
            if "TRIPOS" in line and james_bond==True:
                break
                # Breaks if any new section after the BOND one
            if "@<TRIPOS>BOND" in line:
                whereami_mol2=False
                james_bond=True
                out+=line
                continue
            if whereami_mol2==False:
                out+=line
                continue
            elif whereami_mol2==True:
            ## Residue number check and modification
                ls = line.strip().split()
                atom_index = int(ls[0])
                atom_type = ls[5]
                list_atom_types.append(atom_type)
                out += line 
                continue
        else:
            out+=line

# Now we add the CGenFF alternative atom types
out+="@<TRIPOS>ALT_TYPE\nSYBYL_ALT_TYPE_SET\nSYBYL "
for idx, atom_type in enumerate(list_atom_types):
    out += f"{idx+1} {atom_type} "
out += "\n"

writefile=open(sys.argv[2], "w")
writefile.write(out)
writefile.close()

print("Done!\nWatch out for \'XXX\' or \'DAMN\' in the ALT_TYPE section!")
