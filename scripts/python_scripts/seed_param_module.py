# This module contains functions to help write/fill parameter/input files for SEED.
import os
import sys
import pandas as pd

def read_vdw_params(vdw_params_fn):
    """Read vdw parameters from file.

    Parameters
    ----------
    vdw_params_fn : str
        file name

    Returns
    -------
    vdw_params : pandas.DataFrame
        dataframe containing vdw parameters
    """
    vdw_params = pd.read_csv(vdw_params_fn, sep="\s+", header=None, comment="#", 
                             names=["atom_type", "elem_num", "radius", "eps"],
                             usecols=[1,2,3,4])
    vdw_params = vdw_params.reset_index()
    vdw_params["index"] = vdw_params["index"] + 1
    return vdw_params

def read_atom_weight_params(atom_weight_params_fn):
    atom_weights = pd.read_csv(atom_weight_params_fn, sep="\s+", header=None, comment="#",
                               names=["atom_el", "elem_num", "weight"], usecols=[0,1,2])
    return atom_weights

def read_hb_dist_params(hb_dist_params_fn):

    with open(hb_dist_params_fn, "r") as f:
        lines = f.readlines()
    hb_dist_params = {}
    
    istart = 0
    for i, line in enumerate(lines):
        if not line.strip().startswith("#"):
            istart = i
            break

    default_dist = float(lines[istart].split()[0])
    hb_dist_params["default"] = default_dist
    n_block1 = int(lines[istart+1].split()[0])
    block1 = []
    for i in range(istart+2, istart+2+n_block1):
        ls = [float(x) for x in lines[i].split()[:3]]
        block1.append(ls)
    tmp = pd.DataFrame(block1, columns=["el_num1", "el_num2", "dist"])
    hb_dist_params["block1"] = tmp.astype({"el_num1": int, "el_num2": int, "dist": float})
    n_block2 = int(lines[istart+2+n_block1].split()[0])
    block2 = []
    for i in range(istart+3+n_block1, istart+3+n_block1+n_block2):
        ls = [float(x) for x in lines[i].split()[:3]]
        block2.append(ls)
    tmp = pd.DataFrame(block2, columns=["atom_type1", "atom_type2", "dist"])
    hb_dist_params["block2"] = tmp.astype({"atom_type1": int, "atom_type2": int, "dist": float})

    return hb_dist_params


class SEEDParamSet():
    """Class to hold parameters for SEED input/parameter files.
    """
    attr_list = ["prot_diel", 
                 "kept_vec_ratio", 
                 "write_mol2", 
                 "write_energy", 
                 "max_poses", 
                 "log_out", 
                 "coul_grid", 
                 "vdw_grid", 
                 "desol_grid", 
                 "bump_check_slow", 
                 "bump_check_fast", 
                 "hbond_geometry", 
                 "num_rotations", 
                 "angle_criterion", 
                 "vdw_probe_radius", 
                 "coul_grid_sizes", 
                 "vdw_grid_sizes", 
                 "slow_energy_vdw_cutoff", 
                 "apolar_k", 
                 "solv_grid_sizes", 
                 "water_radius", 
                 "point_density_SAS", 
                 "solv_diel", 
                 "hydrophobicity_map", 
                 "scaling_factors", 
                 "cluster_params", 
                 "gseal1", 
                 "gseal2", 
                 "max_clu_poses", 
                 "print_level", 
                 "vdw_params", 
                 "hbond_params", 
                 "atomic_weights"]
    
    def __init__(self, 
                 prot_diel=2.0,
                 kept_vec_ratio=(1.0, 1.0),
                 write_mol2=(False, True),
                 write_energy=(False,True),
                 max_poses=(5,1),
                 log_out="./outputs/seed.out",
                 coul_grid = ("w", "./scratch/coulomb.grid"),
                 vdw_grid = ("w", "./scratch/vanderwaals.grid"),
                 desol_grid = ("w", "./scratch/desolvation.grid"),
                 bump_check_slow = (2.0, 0.89, 0.6),
                 bump_check_fast = 1.0,
                 hbond_geometry=(50.0, 100),
                 num_rotations=72,
                 angle_criterion=(70.0, 10.0, 1.2, 0.8),
                 vdw_probe_radius=1.83,
                 coul_grid_sizes=(1, 20.0, 0.5),
                 vdw_grid_sizes=(20.0, 0.3),
                 slow_energy_vdw_cutoff=(12.0, 1.0),
                 apolar_k=-0.333333,
                 solv_grid_sizes=(24.0, 0.25),
                 water_radius=1.4,
                 point_density_SAS=500,
                 solv_diel=78.5,
                 hydrophobicity_map=(1.0, 1.0, 1.4, 1.0, 1.0),
                 scaling_factors=(1.0, 1.0, 1.0, 1.0), 
                 cluster_params=None, 
                 gseal1=(0.9,0.4),
                 gseal2=(0.9,0.9),
                 max_clu_poses=20,
                 print_level=(100, 1),
                 vdw_params=None,
                 hbond_params=None,
                 atomic_weights=None):
        """Initialize SEEDParamSet object.
        """
        if cluster_params is None:
            cluster_params = [(6,6,2.0), (7,7,10.0), (8,8,10.0), (16,16,10.0)]
        self.prot_diel = prot_diel
        self.kept_vec_ratio = kept_vec_ratio
        self.write_mol2 = write_mol2
        self.write_energy = write_energy
        self.max_poses = max_poses
        self.log_out = log_out
        self.coul_grid = coul_grid
        self.vdw_grid = vdw_grid
        self.desol_grid = desol_grid
        self.bump_check_slow = bump_check_slow
        self.bump_check_fast = bump_check_fast
        self.hbond_geometry = hbond_geometry
        self.num_rotations = num_rotations
        self.angle_criterion = angle_criterion
        self.vdw_probe_radius = vdw_probe_radius
        self.coul_grid_sizes = coul_grid_sizes
        self.vdw_grid_sizes = vdw_grid_sizes
        self.slow_energy_vdw_cutoff = slow_energy_vdw_cutoff
        self.apolar_k = apolar_k
        self.solv_grid_sizes = solv_grid_sizes
        self.water_radius = water_radius
        self.point_density_SAS = point_density_SAS
        self.solv_diel = solv_diel
        self.hydrophobicity_map = hydrophobicity_map
        self.scaling_factors = scaling_factors
        self.cluster_params = cluster_params
        self.gseal1 = gseal1
        self.gseal2 = gseal2
        self.max_clu_poses = max_clu_poses
        self.print_level = print_level
        self.vdw_params = vdw_params
        self.hbond_params = hbond_params
        self.atomic_weights = atomic_weights
        
        if vdw_params is None or hbond_params is None or atomic_weights is None:
            raise ValueError("vdw_params, hbond_params, and atomic_weights must be specified.")
        
        if isinstance(vdw_params, str):
            self.vdw_params = read_vdw_params(vdw_params)
        else:
            self.vdw_params = vdw_params
        if isinstance(hbond_params, str):
            self.hbond_params = read_hb_dist_params(hbond_params)
        else:
            self.hbond_params = hbond_params
        if isinstance(atomic_weights, str):
            self.atomic_weights = read_atom_weight_params(atomic_weights)
        else:
            self.atomic_weights = atomic_weights
        
        return

    @classmethod
    def from_kw_file(cls, kw_fn, verbose=False):
        kw_dict = {}
        with open(kw_fn, "r") as f:
            for line in f:
                line = line.strip()
                if line.startswith("#") or line == "":
                    continue
                # elif line.startswith("end"):
                #     break
                else:
                    if "#" in line:
                        line = line.split("#")[0]
                    key, val = line.split("=")
                    key = key.strip()
                    
                    if key not in cls.attr_list:
                        if verbose:
                            print(f"WARNING: {key} is not a recognized keyword")
                        continue
                    val = val.strip()

                    if key == "write_mol2" or key == "write_energy":
                        val = tuple([True if x == "y" else False for x in val.split()])
                    
                    elif key in ["max_poses", "print_level"]:
                        val = tuple([int(x) for x in val.split()])
                    
                    elif key in ["kept_vec_ratio", "bump_check_slow", "angle_criterion", 
                                 "vdw_grid_sizes", "slow_energy_vdw_cutoff", "solv_grid_sizes", "hydrophobicity_map", 
                                 "scaling_factors", "gseal1", "gseal2"]: 
                        val = tuple([float(x) for x in val.split()])
                    
                    elif key == "coul_grid_sizes":
                        vs = val.split()
                        val = (int(vs[0]), float(vs[1]), float(vs[2]))
                    
                    elif key == "hbond_geometry":
                        vs = val.split()
                        val = (float(vs[0]), int(vs[1]))
                    
                    elif key in ["coul_grid", "vdw_grid", "desol_grid"]:
                        val = tuple([str(x) for x in val.split()])

                    elif key in ["num_rotations", "point_density_SAS", "max_clu_poses"]:
                        val = int(val)
                    
                    elif key in ["prot_diel", "bump_check_fast", "vdw_probe_radius", "apolar_k", "water_radius", "solv_diel"]:
                        val = float(val)
                    ## base case
                    # else:
                    #     val = str(val)
                    kw_dict[key] = val

        return cls(**kw_dict)
    
    def write_par_str(self):
        """Write parameter file string.
        """
        par_str = "#seed.param v4.0\n"
        for key, val in self.__dict__.items():
            if key == "water_radius":
                par_str += f"{val} {self.point_density_SAS} {self.solv_diel}\n#\n"
            elif key in ["point_density_SAS", "solv_diel"]:
                continue
            elif not isinstance(val, pd.DataFrame) and key != "hbond_params" and key != "cluster_params":
                if not isinstance(val, tuple):
                    par_str += "{}\n#\n".format(val)
                elif key == "write_mol2" or key == "write_energy":
                    tmp = " ".join(["y" if x == True else "n" for x in val])
                    par_str += "{}\n#\n".format(tmp)
                else:
                    par_str += "{}\n#\n".format(" ".join([str(x) for x in val]))
            elif key == "cluster_params":
                par_str += "{}\n".format(len(val))
                for v in val:
                    par_str += " ".join([str(x) for x in v]) + "\n"
                par_str += "#\n"
            elif isinstance(val, pd.DataFrame):
                par_str += "{}\n".format(len(val))
                par_str += val.to_string(header=False, index=False) + "\n"
                # for i, row in val.iterrows():
                    # par_str += " ".join([str(x) for x in row]) + "\n"
                par_str += "#\n"
            elif key == "hbond_params":
                par_str += "{}\n".format(val["default"])
                par_str += "{}\n".format(len(val["block1"]))
                par_str += val['block1'].to_string(header=False, index=False) + "\n"
                # for i, row in val["block1"].iterrows():
                    # par_str += " ".join([f"{x:<5}" for x in row]) + "\n"
                par_str += "{}\n".format(len(val["block2"]))
                par_str += val['block2'].to_string(header=False, index=False) + "\n"
                # for i, row in val["block2"].iterrows():
                    # par_str += " ".join([f"{x:<5}" for x in row]) + "\n"
                par_str += "#\n"
        par_str += "end\n"
            
        return par_str

    def write_par_file(self, par_fn):
        with open(par_fn, "w") as f:
            f.write(self.write_par_str())

    def __repr__(self):
        repr_str = "SEEDParamSet("
        for key, val in self.__dict__.items():
            if isinstance(val, pd.DataFrame):
                repr_str += "{}=pd.DataFrame, \n".format(key)
            elif key == "hbond_params":
                repr_str += "{}=dict, \n".format(key)
            else:
                repr_str += "{}={}, \n".format(key, val)
        repr_str = repr_str[:-3] + ")"
        return repr_str

class SEEDKWParamSet():
    """SEED keyword parameter set.
    """
    attr_list = ["do_mc",
                "mc_temp",
                "mc_max_xyz_step",
                "mc_max_rot_step",
                "mc_rot_freq",
                "mc_rot_fine_freq",
                "mc_xyz_fine_freq",
                "mc_niter",
                "mc_sa_alpha",
                "mc_rseed",
                "do_sd",
                "do_gradient_check",
                "sd_max_iter",
                "sd_eps_grms",
                "sd_alpha_xyz",
                "sd_alpha_rot",
                "sd_learning_rate",
                "do_softcore",
                "sc_alpha",
                "sc_lambda",
                "sc_avg_diam",
                "sc_do_shift"]

    def __init__(self,
                 do_mc="n",
                 mc_temp=600.0,
                 mc_max_xyz_step=(0.7, 0.1),
                 mc_max_rot_step=(3.0, 0.5),
                 mc_rot_freq=0.5,
                 mc_rot_fine_freq=0.75,
                 mc_xyz_fine_freq=0.75,
                 mc_niter=(500, 5),
                 mc_sa_alpha=0.995,
                 mc_rseed=-1,
                 do_sd="n",
                 do_gradient_check="n",
                 sd_max_iter=20,
                 sd_eps_grms=0.02,
                 sd_alpha_xyz=0.1,
                 sd_alpha_rot=0.02,
                 sd_learning_rate=0.1,
                 do_softcore="n",
                 sc_alpha=1.0,
                 sc_lambda=0.5,
                 sc_avg_diam=3.4,
                 sc_do_shift="y") -> None:
        
        self.do_mc = do_mc
        self.mc_temp = mc_temp
        self.mc_max_xyz_step = mc_max_xyz_step
        self.mc_max_rot_step = mc_max_rot_step
        self.mc_rot_freq = mc_rot_freq
        self.mc_rot_fine_freq = mc_rot_fine_freq
        self.mc_xyz_fine_freq = mc_xyz_fine_freq
        self.mc_niter = mc_niter
        self.mc_sa_alpha = mc_sa_alpha
        self.mc_rseed = mc_rseed
        self.do_sd = do_sd
        self.do_gradient_check = do_gradient_check
        self.sd_max_iter = sd_max_iter
        self.sd_eps_grms = sd_eps_grms
        self.sd_alpha_xyz = sd_alpha_xyz
        self.sd_alpha_rot = sd_alpha_rot
        self.sd_learning_rate = sd_learning_rate
        self.do_softcore = do_softcore
        self.sc_alpha = sc_alpha
        self.sc_lambda = sc_lambda
        self.sc_avg_diam = sc_avg_diam
        self.sc_do_shift = sc_do_shift
    
    @classmethod
    def from_kw_file(cls, kw_fn, verbose=False):
        kw_dict = {}
        with open(kw_fn, "r") as f:
            for line in f:
                line = line.strip()
                if line.startswith("#") or line == "":
                    continue
                # elif line.startswith("end"):
                #     break
                else:
                    if "#" in line:
                        line = line.split("#")[0]
                    key, val = line.split("=")
                    key = key.strip()
                    if key not in cls.attr_list:
                        if verbose:
                            print(f"WARNING: {key} is not a recognized keyword")
                        continue
                    val = val.strip()
                    if key == "mc_max_xyz_step" or key == "mc_max_rot_step":
                        val = tuple([float(x) for x in val.split()])
                    elif key == "mc_niter":
                        val = tuple([int(x) for x in val.split()])
                    elif key == "mc_rseed" or key == "sd_max_iter":
                        val = int(val)
                    elif key == "do_mc" or key == "do_sd" or key == "do_gradient_check" or key == "do_softcore" or key == "sc_do_shift":
                        val = str(val)
                    else:
                        val = float(val)
                    kw_dict[key] = val
        return cls(**kw_dict)
    
    def write_kw_str(self):
        kw_str = ""
        for key, val in self.__dict__.items():
            if isinstance(val, tuple):
                kw_str += "{} = {}\n".format(key, " ".join([str(x) for x in val]))
            else:
                kw_str += "{} = {}\n".format(key, val)
        return kw_str

    def write_kw_file(self, kw_fn):
        with open(kw_fn, "w") as f:
            f.write(self.write_kw_str())

    def __repr__(self):
        repr_str = "SEEDKWParamSet("
        for key, val in self.__dict__.items():
            repr_str += "{}={}, \n".format(key, val)
        repr_str = repr_str[:-3] + ")"
        return repr_str
        

class SEEDInputSet():
    """class to hold input file for SEED
    """
    def __init__(self,
                 par_fn,
                 par_kw_fn,
                 rec_fn,
                 res_list,
                 anchor_points=[],
                 metal_points=[],
                 spherical_cutoff=("n"),
                 running_mode="d",
                 lib_fn=None,
                 docking_mode="b",
                 energy_cutoffs=(0.0, 0.0),
                 reading_mode="single"):
        
        self.par_fn = par_fn
        self.par_kw_fn = par_kw_fn
        self.rec_fn = rec_fn
        self.res_list = res_list
        self.anchor_points = anchor_points
        self.metal_points = metal_points
        self.spherical_cutoff = spherical_cutoff
        self.running_mode = running_mode
        if lib_fn is None:
            raise ValueError("lib_fn must be specified.")
        self.lib_fn = lib_fn
        self.docking_mode = docking_mode
        self.energy_cutoffs = energy_cutoffs
        self.reading_mode = reading_mode
    
    def write_inp_str(self) -> str:
        """Write input file string.
        """
        inp_str = ""
        for key, val in self.__dict__.items():
            
            if key == "par_fn":
                inp_str += "{}\n".format(val)
                inp_str += f"{self.par_kw_fn}\n#\n"

            elif key == "res_list":
                inp_str += "{}\n".format(len(val))
                inp_str += "\n".join([str(x) for x in val]) + "\n#\n"
            
            elif key == "anchor_points":
                inp_str += "{}\n".format(len(val))
                for v in val:
                    inp_str += "{}\n".format(" ".join([str(x) for x in v]))
                inp_str += "#\n"
            
            elif key == "metal_points":
                inp_str += "{}\n".format(len(val))
                for v in val:
                    inp_str += "{}\n".format(" ".join([str(x) for x in v]))
                inp_str += "#\n"

            elif key == "spherical_cutoff":
                inp_str += "{}\n".format(" ".join([str(x) for x in val]))
                inp_str += "#\n"

            elif key == "lib_fn":
                inp_str += f"{self.running_mode}\n"
                inp_str += f"{val} {self.docking_mode} {self.energy_cutoffs[0]} {self.energy_cutoffs[1]}\n"
                inp_str += f"{self.reading_mode}\n#\n"
            
            
            elif key == "docking_mode" or key == "energy_cutoffs" or key == "reading_mode" or key == "running_mode" or key == "par_kw_fn":
                continue

            else:
                inp_str += "{}\n#\n".format(val)
        inp_str += "end\n"

        return inp_str
    
    def write_inp_file(self, inp_fn):
        with open(inp_fn, "w") as f:
            f.write(self.write_inp_str())
    
    def __repr__(self) -> str:
        repr_str = "SEEDInputSet("
        for key, val in self.__dict__.items():
            repr_str += "{}={}, \n".format(key, val)
        repr_str = repr_str[:-3] + ")"
        return repr_str
    
##############################################
if __name__ == "__main__":

    pars = SEEDParamSet(vdw_params="../../params/seed_vdw_amber.par",
                        hbond_params="../../params/hbond/cgenff_hbond_dist.par",
                        atomic_weights="../../params/atom_weights.par")
    pars.write_par_file("test/test.par")

    inps = SEEDInputSet(par_fn="seed.par",
                        par_kw_fn="seed_kw.par",
                        rec_fn="rec.mol2",
                        res_list=[181,182,185,225],
                        anchor_points=[(0.0, 0.0, 0.0), (1.0, 1.0, 1.0), (2.0, 2.0, 2.0), (3.0, 3.0, 3.0), (4.0, 4.0, 4.0)],
                        metal_points=[(53, 0.0, 0.0, 0.0), (53, 1.0, 1.0, 1.0)],
                        spherical_cutoff=("y", 10.0, 20.0),
                        lib_fn="test_worked.mol2")
    inps.write_inp_file("test/test.inp")

    kw_pars = SEEDKWParamSet()
    kw_pars.write_kw_file("test/test_kw.par")
    new_kw_pars = SEEDKWParamSet.from_kw_file("../../test_cases/seed4_kw.par")
    new_kw_pars.write_kw_file("test/test_kw2.par")

    pars_from_kw = SEEDParamSet.from_kw_file("test/test_kw_input.par")
    pars_from_kw.write_par_file("test/test_from_kw.par")

    # Mixed:
    mixed_pars = SEEDParamSet.from_kw_file("test/test_kw_input_full.par")
    mixed_pars = SEEDKWParamSet.from_kw_file("test/test_kw_input_full.par")