1      H      1     0.22450 0.046000  # polar H
2      HC     1     0.22450 0.046000  # N-ter H
3      HA     1     1.32000 0.022000  # nonpolar H
4      HP     1     1.35820 0.030000  # aromatic H
5      HB1    1     1.32000 0.022000  # backbone H
6      HR1    1     0.90000 0.046000  # his he1, (+) his HG,HD2
7      HR2    1     0.70000 0.046000  # (+) his HE1
8      HR3    1     1.46800 0.007800  # neutral his HG, HD2
9      HS     1     0.45000 0.100000  # thiol hydrogen
10     HE1    1     1.25000 0.031000  # for alkene; RHC=CR
11     HE2    1     1.26000 0.026000  # for alkene; H2C=CR
12     C      6     2.00000 0.110000  # carbonyl C, peptide backbone
13     CA     6     1.99240 0.070000  # aromatic C
14     CT1    6     2.0000  0.0320    # aliphatic sp3 C for CH
15     CT2    6     2.010   0.0560    # aliphatic sp3 C for CH2
16     CT3    6     2.040   0.0780    # aliphatic sp3 C for CH3
17     CPH1   6     1.80000 0.050000  # his CG and CD2 carbons
18     CPH2   6     1.80000 0.050000  # his CE1 carbon
19     CPT    6     1.80000 0.090000  # trp C between rings
20     CY     6     1.99240 0.070000  # TRP C in pyrrole ring
21     CP1    6     2.27500 0.020000  # tetrahedral C (proline CA)
22     CP2    6     2.17500 0.055000  # tetrahedral C (proline CB/CG)
23     CP3    6     2.17500 0.055000  # tetrahedral C (proline CD)
24     CC     6     2.00000 0.070000  # carbonyl C, asn,asp,gln,glu,cter,ct2
25     CD     6     2.00000 0.070000  # carbonyl C, pres aspp,glup,ct1
26     CS     6     2.20000 0.110000  # thiolate carbon
27     CE1    6     2.09000 0.068000  # for alkene; RHC=CR
28     CE2    6     2.08000 0.064000  # for alkene; H2C=CR
29     CAI    6     1.99000 0.073000  # aromatic C next to CPT in trp
30     N      7     1.85000 0.200000  # proline N
31     NR1    7     1.85000 0.200000  # neutral his protonated ring nitrogen
32     NR2    7     1.85000 0.200000  # neutral his unprotonated ring nitrogen
33     NR3    7     1.85000 0.200000  # charged his ring nitrogen
34     NH1    7     1.85000 0.200000  # peptide nitrogen
35     NH2    7     1.85000 0.200000  # amide nitrogen
36     NH3    7     1.85000 0.200000  # ammonium nitrogen
37     NC2    7     1.85000 0.200000  # guanidinium nitrogen
38     NY     7     1.85000 0.200000  # TRP N in pyrrole ring
39     NP     7     1.85000 0.200000  # Proline ring NH2+ (N-terminal)
40     O      8     1.70000 0.120000  # carbonyl oxygen
41     OB     8     1.70000 0.120000  # carbonyl oxygen in acetic acid
42     OC     8     1.70000 0.120000  # carboxylate oxygen  
43     OH1    8     1.77000 0.152100  # hydroxyl oxygen  
44     OS     8     1.77000 0.152100  # ester oxygen  
45     S      16    2.00000 0.450000  # sulphur  
46     SM     16    1.97500 0.380000  # sulfur C-S-S-C type  
47     SS     16    2.20000 0.470000  # thiolate sulfur  
48     ALG1   13    2.00000 0.650000  # CGENFF 4 parameters  Aluminum, for ALF4, AlF4-
49     BRGA1  35    1.97    0.48      # CGENFF 4 parameters   BRET, bromoethane
50     BRGA2  35    2.05    0.53      # CGENFF 4 parameters   DBRE, 1,1-dibromoethane
51     BRGA3  35    2.0     0.54       # CGENFF 4 parameters   TBRE, 1,1,1-dibromoethane
52     BRGR1  35    1.98    0.32       # CGENFF 4 parameters   BROB, bromobenzene
53     CG1N1  6     1.87    0.18       # CGENFF 4 parameters   C for cyano group
54     CG1T1  6     1.84    0.167       # CGENFF 4 parameters   internal alkyne R-C#C
55     CG1T2  6     1.9925  0.1032       # CGENFF 4 parameters   terminal alkyne H-C#C
56     CG251O 6     2.09    0.068       # CGENFF 4 parameters  same as CG2D1O but in 5-membered ring with exocyclic double bond
57     CG252O 6     2.09    0.068       # CGENFF 4 parameters  same as CG2D2O but in 5-membered ring with exocyclic double bond
58     CG25C1 6     2.09    0.068       # CGENFF 4 parameters  same as CG2DC1 but in 5-membered ring with exocyclic double bond
59     CG25C2 6     2.09    0.068       # CGENFF 4 parameters  same as CG2DC2 but in 5-membered ring with exocyclic double bond
60     CG2D1  6     2.09    0.068       # CGENFF 4 parameters  alkene; RHC= ; imine C
61     CG2D1O 6     2.09    0.068       # CGENFF 4 parameters  double bond carbon adjacent to heteroatom. In conjugated systems, the atom to which it is double bonded must be CG2DC1.
62     CG2D2  6     2.08    0.064       # CGENFF 4 parameters  alkene; H2C=
63     CG2D2O 6     2.09    0.068       # CGENFF 4 parameters  double bond carbon adjacent to heteroatom. In conjugated systems, the atom to which it is double bonded must be CG2DC2.
64     CG2DC1 6     2.09    0.068       # CGENFF 4 parameters  double bond carbon adjacent to heteroatom. In conjugated systems, the atom to which it is double bonded must be CG2DC1.
65     CG2DC2 6     2.09    0.068       # CGENFF 4 parameters  double bond carbon adjacent to heteroatom. In conjugated systems, the atom to which it is double bonded must be CG2DC2.
66     CG2DC3 6     2.08    0.064       # CGENFF 4 parameters  conjugated alkenes, H2C=
67     CG2N1  6     2.0     0.11       # CGENFF 4 parameters   conjugated C in guanidine/guanidinium
68     CG2N2  6     2.0     0.11       # CGENFF 4 parameters   conjugated C in amidinium cation
69     CG2O1  6     2.0     0.11       # CGENFF 4 parameters   carbonyl C: amides
70     CG2O2  6     1.7     0.098       # CGENFF 4 parameters   carbonyl C: esters, [neutral] carboxylic acids
71     CG2O3  6     2.0     0.07       # CGENFF 4 parameters   carbonyl C: [negative] carboxylates
72     CG2O4  6     1.8     0.06       # CGENFF 4 parameters   carbonyl C: aldehydes
73     CG2O5  6     2.0     0.09       # CGENFF 4 parameters   carbonyl C: ketones
74     CG2O6  6     2.0     0.07       # CGENFF 4 parameters   carbonyl C: urea, carbonate
75     CG2O7  6     1.563   0.058       # CGENFF 4 parameters   CO2 carbon
76     CG2R51 6     2.1    0.05       # CGENFF 4 parameters   5-mem ring, his CG, CD2(0), trp
77     CG2R52 6     2.2   0.02       # CGENFF 4 parameters   5-mem ring, double bound to N, PYRZ, pyrazole
78     CG2R53 6     2.2   0.02       # CGENFF 4 parameters   5-mem ring, double bound to N and adjacent to another heteroatom, purine C8, his CE1 (0,+1), 2PDO, kevo
79     CG2R57 6     2.1    0.05       # CGENFF 4 parameters   5-mem ring, bipyrroles
80     CG2R61 6     1.9924    0.07       # CGENFF 4 parameters   6-mem aromatic C
81     CG2R62 6     1.9    0.09       # CGENFF 4 parameters   6-mem aromatic C for protonated pyridine (NIC) and rings containing carbonyls (see CG2R63) (NA)
82     CG2R63 6     1.9    0.1       # CGENFF 4 parameters   6-mem aromatic C for protonated pyridine (NIC) and rings containing carbonyls (see CG2R63) (NA)
83     CG2R64 6     2.1    0.04       # CGENFF 4 parameters   6-mem aromatic amidine and guanidine carbon (between 2 or 3 Ns and double-bound to one of them), NA, PYRM
84     CG2R66 6     1.9    0.07       # CGENFF 4 parameters   6-mem aromatic carbon bound to F
85     CG2R67 6     1.9924    0.07       # CGENFF 4 parameters   6-mem aromatic carbon of biphenyl
86     CG2R71 6     1.9948    0.067       # CGENFF 4 parameters   7-mem ring arom C, AZUL, azulene, kevo
87     CG2RC0 6     1.86    0.099       # CGENFF 4 parameters   6/5-mem ring bridging C, guanine C4,C5, trp
88     CG2RC7 6     1.86    0.099       # CGENFF 4 parameters   sp2 ring connection with single bond(
89     CG301  6     2.0   0.032       # CGENFF 4 parameters   aliphatic C, no hydrogens, neopentane
90     CG302  6     2.3    0.02       # CGENFF 4 parameters   aliphatic C, no hydrogens, trifluoromethyl
91     CG311  6     2.0   0.032       # CGENFF 4 parameters   aliphatic C with 1 H, CH
92     CG312  6     2.05    0.042       # CGENFF 4 parameters   aliphatic C with 1 H, difluoromethyl
93     CG314  6     2.165    0.031       # CGENFF 4 parameters   aliphatic C with 1 H, adjacent to positive N (PROT NTER) (+)
94     CG321  6     2.01    0.056       # CGENFF 4 parameters   aliphatic C for CH2
95     CG322  6     1.9    0.06       # CGENFF 4 parameters   aliphatic C for CH2, monofluoromethyl
96     CG323  6     2.2   0.11       # CGENFF 4 parameters   aliphatic C for CH2, thiolate carbon
97     CG324  6     2.175    0.055       # CGENFF 4 parameters   aliphatic C for CH2, adjacent to positive N (piperidine) (+)
98     CG331  6     2.05    0.078       # CGENFF 4 parameters   aliphatic C for methyl group (-CH3)
99     CG334  6     2.215    0.077       # CGENFF 4 parameters   aliphatic C for methyl group (-CH3), adjacent to positive N (PROT NTER) (+)
100    CG3AM0 6     1.97    0.07       # CGENFF 4 parameters   aliphatic C for CH3, NEUTRAL trimethylamine methyl carbon (#)
101    CG3AM1 6     1.98    0.078       # CGENFF 4 parameters   aliphatic C for CH3, NEUTRAL dimethylamine methyl carbon (#)
102    CG3AM2 6     1.99    0.08       # CGENFF 4 parameters   aliphatic C for CH3, NEUTRAL methylamine methyl carbon (#)
103    CG3C31 6     2.01    0.056       # CGENFF 4 parameters   cyclopropyl carbon
104    CG3C41 6     2.02   0.065       # CGENFF 4 parameters   cyclobutyl carbon
105    CG3C50 6     2.01    0.036       # CGENFF 4 parameters   5-mem ring aliphatic quaternary C (cholesterol, bile acids)
106    CG3C51 6     2.01    0.036       # CGENFF 4 parameters   5-mem ring aliphatic CH  (proline CA, furanoses)
107    CG3C52 6     2.02   0.06       # CGENFF 4 parameters   5-mem ring aliphatic CH2 (proline CB/CG/CD, THF, deoxyribose)
108    CG3C53 6     2.175    0.035       # CGENFF 4 parameters   5-mem ring aliphatic CH  adjacent to positive N (proline.H+ CA) (+)
109    CG3C54 6     2.185    0.059       # CGENFF 4 parameters   5-mem ring aliphatic CH2 adjacent to positive N (proline.H+ CD) (+)
110    CG3RC1 6     2.0   0.032       # CGENFF 4 parameters   bridgehead in bicyclic systems containing at least one 5-membered or smaller ring
111    CLGA1  17    1.91    0.343       # CGENFF 4 parameters   CLET, DCLE, chloroethane, 1,1-dichloroethane
112    CLGA3  17    1.91    0.31       # CGENFF 4 parameters   TCLE, 1,1,1-trichloroethane
113    CLGR1  17    1.86    0.23       # CGENFF 4 parameters   CHLB, chlorobenzene
114    FGA1   9    1.63    0.135       # CGENFF 4 parameters  aliphatic fluorine, monofluoro
115    FGA2   9    1.63    0.105       # CGENFF 4 parameters  aliphatic fluorine, difluoro
116    FGA3   9    1.6    0.097       # CGENFF 4 parameters  aliphatic fluorine, trifluoro
117    FGP1   9    1.6    0.097       # CGENFF 4 parameters  anionic F, for ALF4 AlF4-
118    FGR1   9    1.7    0.12       # CGENFF 4 parameters  aromatic flourine
119    HGA1   1    1.34    0.045       # CGENFF 4 parameters  alphatic proton, CH
120    HGA2   1    1.34    0.035       # CGENFF 4 parameters  alphatic proton, CH2
121    HGA3   1    1.34    0.024       # CGENFF 4 parameters  alphatic proton, CH3
122    HGA4   1    1.25    0.031       # CGENFF 4 parameters  alkene proton; RHC=
123    HGA5   1    1.26    0.026       # CGENFF 4 parameters  alkene proton; H2C=CR
124    HGA6   1    1.32   0.028       # CGENFF 4 parameters  aliphatic H on fluorinated C, monofluoro
125    HGA7   1    1.3    0.03       # CGENFF 4 parameters  aliphatic H on fluorinated C, difluoro
126    HGAAM0 1    1.28    0.028       # CGENFF 4 parameters  aliphatic H, NEUTRAL trimethylamine (#)
127    HGAAM1 1    1.28    0.028       # CGENFF 4 parameters  aliphatic H, NEUTRAL dimethylamine (#)
128    HGAAM2 1    1.26    0.04       # CGENFF 4 parameters  aliphatic H, NEUTRAL methylamine (#)
129    HGP1   1    0.2245    0.046       # CGENFF 4 parameters  polar H
130    HGP2   1    0.2245    0.046       # CGENFF 4 parameters  polar H, +ve charge
131    HGP3   1    0.45    0.1       # CGENFF 4 parameters  polar H, thiol
132    HGP4   1    0.2245    0.046       # CGENFF 4 parameters  polar H, neutral conjugated -NH2 group (NA bases)
133    HGP5   1    0.7    0.046       # CGENFF 4 parameters  polar H on quarternary ammonium salt (choline)
134    HGPAM1 1    0.875    0.009       # CGENFF 4 parameters  polar H, NEUTRAL dimethylamine (#), terminal alkyne H
135    HGPAM2 1    0.875    0.01       # CGENFF 4 parameters  polar H, NEUTRAL methylamine (#)
136    HGPAM3 1    0.87    0.012       # CGENFF 4 parameters  polar H, NEUTRAL ammonia (#)
137    HGR51  1    1.3582    0.03       # CGENFF 4 parameters  nonpolar H, neutral 5-mem planar ring C, LJ based on benzene
138    HGR52  1    0.9    0.046       # CGENFF 4 parameters  Aldehyde H, formamide H (RCOH); nonpolar H, neutral 5-mem planar ring C adjacent to heteroatom or + charge
139    HGR53  1    0.7    0.046       # CGENFF 4 parameters  nonpolar H, +ve charge HIS he1(+1)
140    HGR61  1    1.3582    0.03       # CGENFF 4 parameters  aromatic H
141    HGR62  1    1.1    0.046       # CGENFF 4 parameters  nonpolar H, neutral 6-mem planar ring C adjacent to heteroatom
142    HGR63  1    0.9    0.046       # CGENFF 4 parameters  nonpolar H, NAD+ nicotineamide all ring CH hydrogens
143    HGR71  1    1.3582    0.03       # CGENFF 4 parameters  nonpolar H, neutral 7-mem arom ring, AZUL, azulene, kevo
144    IGR1   53    2.24    0.52       # CGENFF 4 parameters  IODB, iodobenzene
145    LPH    0    0    0       # CGENFF 4 parameters  Lone pair for halogens
146    NG1T1  7    1.79    0.18       # CGENFF 4 parameters  N for cyano group
147    NG2D1  7    1.85    0.2       # CGENFF 4 parameters  N for neutral imine/Schiff's base (C=N-R, acyclic amidine, gunaidine)
148    NG2O1  7    1.85    0.2       # CGENFF 4 parameters  NITB, nitrobenzene
149    NG2P1  7    1.85    0.2       # CGENFF 4 parameters  N for protonated imine/Schiff's base (C=N(+)H-R, acyclic amidinium, guanidinium)
150    NG2R43 7    1.85    0.2       # CGENFF 4 parameters  amide in 4-memebered ring (planar), AZDO, lsk
151    NG2R50 7    1.85    0.2       # CGENFF 4 parameters  double bound neutral 5-mem planar ring, purine N7
152    NG2R51 7    1.85    0.2       # CGENFF 4 parameters  single bound neutral 5-mem planar (all atom types sp2) ring, his, trp pyrrole (fused)
153    NG2R52 7    1.85    0.2       # CGENFF 4 parameters  protonated schiff base, amidinium, guanidinium in 5-membered ring, HIS, 2HPP, kevo
154    NG2R53 7    1.85    0.2       # CGENFF 4 parameters  amide in 5-memebered NON-SP2 ring (slightly pyramidized), 2PDO, kevo
155    NG2R57 7    1.85    0.2       # CGENFF 4 parameters  5-mem ring, bipyrroles
156    NG2R60 7    1.89    0.06       # CGENFF 4 parameters  double bound neutral 6-mem planar ring, pyr1, pyzn
157    NG2R61 7    1.85    0.2       # CGENFF 4 parameters  single bound neutral 6-mem planar ring imino nitrogen; glycosyl linkage
158    NG2R62 7    2.06    0.05       # CGENFF 4 parameters  double bound 6-mem planar ring with heteroatoms in o or m, pyrd, pyrm
159    NG2R67 7    1.85    0.2       # CGENFF 4 parameters  6-mem planar ring substituted with 6-mem planar ring (N-phenyl pyridinones etc.)
160    NG2RC0 7    1.85    0.2       # CGENFF 4 parameters  6/5-mem ring bridging N, indolizine, INDZ, kevo
161    NG2S0  7    1.85    0.2       # CGENFF 4 parameters  N,N-disubstituted amide, proline N (CO=NRR')
162    NG2S1  7    1.85    0.2       # CGENFF 4 parameters  peptide nitrogen (CO=NHR)
163    NG2S2  7    1.85    0.2       # CGENFF 4 parameters  terminal amide nitrogen (CO=NH2)
164    NG2S3  7    1.85    0.2       # CGENFF 4 parameters  external amine ring nitrogen (planar/aniline), phosphoramidate
165    NG301  7    2.0   0.035       # CGENFF 4 parameters  neutral trimethylamine nitrogen
166    NG311  7    2.0   0.045       # CGENFF 4 parameters  neutral dimethylamine nitrogen
167    NG321  7    1.99    0.06       # CGENFF 4 parameters  neutral methylamine nitrogen
168    NG331  7    1.98    0.07       # CGENFF 4 parameters  neutral ammonia nitrogen
169    NG3C51 7    1.85    0.2       # CGENFF 4 parameters  secondary sp3 amine in 5-membered ring
170    NG3N1  7    2.05    0.06       # CGENFF 4 parameters  N in hydrazine, HDZN
171    NG3P0  7    1.85    0.2       # CGENFF 4 parameters  quarternary N+, choline
172    NG3P1  7    1.85    0.2       # CGENFF 4 parameters  tertiary NH+ (PIP)
173    NG3P2  7    1.85    0.2       # CGENFF 4 parameters  secondary NH2+ (proline)
174    NG3P3  7    1.85    0.2       # CGENFF 4 parameters  primary NH3+, phosphatidylethanolamine
175    OG2D1  8    1.7    0.12       # CGENFF 4 parameters  carbonyl O: amides, esters, [neutral] carboxylic acids, aldehydes, uera
176    OG2D2  8    1.7    0.12       # CGENFF 4 parameters  carbonyl O: negative groups: carboxylates, carbonate
177    OG2D3  8    1.7    0.05       # CGENFF 4 parameters  carbonyl O: ketones
178    OG2D4  8    1.7    0.12       # CGENFF 4 parameters  6-mem aromatic carbonyl oxygen (nucleic bases)
179    OG2D5  8    1.692   0.165       # CGENFF 4 parameters  CO2 oxygen
180    OG2N1  8    1.7    0.12       # CGENFF 4 parameters  NITB, nitrobenzene
181    OG2P1  8    1.7    0.12       # CGENFF 4 parameters  =O in phosphate or sulfate
182    OG2R50 8    1.7    0.12       # CGENFF 4 parameters  FURA, furan
183    OG301  8    1.65    0.1       # CGENFF 4 parameters  ether -O- 
184    OG302  8    1.65    0.1       # CGENFF 4 parameters  ester -O-
185    OG303  8    1.65    0.1       # CGENFF 4 parameters  phosphate/sulfate ester oxygen
186    OG304  8    1.65    0.1       # CGENFF 4 parameters  linkage oxygen in pyrophosphate/pyrosulphate
187    OG311  8    1.765    0.1921       # CGENFF 4 parameters  hydroxyl oxygen
188    OG312  8    1.75    0.12       # CGENFF 4 parameters  ionized alcohol oxygen
189    OG3C31 8    1.65    0.1       # CGENFF 4 parameters  epoxide oxygen, 1EOX, 1BOX, sc
190    OG3C51 8    1.65    0.1       # CGENFF 4 parameters  5-mem furanose ring oxygen (ether)
191    OG3C61 8    1.65    0.1       # CGENFF 4 parameters  DIOX, dioxane, ether in 6-membered ring 
192    OG3R60 8    1.65    0.1       # CGENFF 4 parameters  O in 6-mem cyclic enol ether (PY01, PY02) or ester
193    PG0    15    2.15    0.585       # CGENFF 4 parameters  neutral phosphate
194    PG1    15    2.15    0.585       # CGENFF 4 parameters  phosphate -1
195    PG2    15    2.15    0.585       # CGENFF 4 parameters  phosphate -2
196    SG2D1  16    2.05    0.565       # CGENFF 4 parameters  thiocarbonyl S
197    SG2R50 16    2.0   0.45       # CGENFF 4 parameters  THIP, thiophene
198    SG301  16    1.975    0.38       # CGENFF 4 parameters  sulfur C-S-S-C type
199    SG302  16    2.2   0.47       # CGENFF 4 parameters  thiolate sulfur (-1)
200    SG311  16    2.0   0.45       # CGENFF 4 parameters  sulphur, SH, -S-
201    SG3O1  16    2.1    0.47       # CGENFF 4 parameters  sulfate -1 sulfur
202    SG3O2  16    2.0   0.35       # CGENFF 4 parameters  neutral sulfone/sulfonamide sulfur
203    SG3O3  16    2.0   0.35       # CGENFF 4 parameters  neutral sulfoxide sulfur
204    HB2    1     1.3400  0.0280    # aliphatic backbone H, to CT2 
205    HA1    1     1.3400  0.045     # alkane, CH, new LJ params (see toppar_all22_prot_aliphatic_c27.str)
206    HA2    1     1.3400  0.034     # alkane, CH2, new LJ params (see toppar_all22_prot_aliphatic_c27.str)
207    HA3    1     1.3400  0.024     # alkane, CH3, new LJ params (see toppar_all22_prot_aliphatic_c27.str)
208    CT     6     2.275   0.020     # aliphatic sp3 C, new LJ params, no hydrogens, see retinol stream file for parameters
209    CT2A   6     2.010   0.056     # from CT2 (GLU, HSP chi1/chi2 fitting) 05282010, zhu
210    HX     1     0.2245  0.0460    # hydroxide hydrogen
211    OX     8     1.7000  0.1200    # hydroxide oxygen
212    LIT    3     1.2975  0.00233   # Lithium ion    
213    SOD    11    1.41075 0.0469    # Sodium Ion
214    MG     12    1.18500 0.0150    # Magnesium Ion
215    POT    19    1.76375 0.0870    # Potassium Ion
216    CAL    20    1.367   0.120     # Calcium Ion  
217    RUB    44    1.90    0.15      # Rubidium Ion
218    CES    55    2.100   0.1900    # Cesium Ion
219    BAR    56    1.890   0.150     # Barium Ion
220    ZN     30    1.0900  0.2500    # zinc (II) cation
221    CAD    48    1.3570  0.1200    # cadmium (II) cation
222    CLA    17    2.27    0.150     # Chloride Ion 
223    CT     6     2.275   0.020     # TIP3P
224    OT     8     1.7682  0.1521    # TIP3P
225    HT     1     0.2245  0.0460    # TIP3P
226    SG2P1  16    2.0937  0.6308    # mono-thio S-P bond
227    OG2S1  8     1.6796  0.1423    # mono-thio S-P bond
228    SG2P2  16    2.0546  0.6199    # di-thio S-P bond
